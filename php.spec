%global   apiver      20230831
%global   zendver     20230831
%global   pdover      20170320
%undefine _strict_symbol_defs_build
%global _hardened_build 1
%global embed_version   8.3
%global mysql_sock %(mysql_config --socket 2>/dev/null || echo /var/lib/mysql/mysql.sock)
%{!?runselftest: %global runselftest 1}
%global mysql_config %{_libdir}/mysql/mysql_config


%{!?_httpd_mmn:        %{expand: %%global _httpd_mmn        %%(cat %{_includedir}/httpd/.mmn 2>/dev/null || echo 0-0)}}

%global with_zts        0
%global with_firebird   0
%global with_freetds    0
%global with_sodium     1
%global with_pspell     0
%global with_tidy       1
%global with_db4        0
%global with_qdbm       0
%global with_imap       0
%global with_modphp     1
%global with_lmdb       1
%global with_sodium     1
%global upver           8.3.17


Name:      php
Version:   %{upver}
Release:   2
Summary:   PHP scripting language for creating dynamic web sites
License:   PHP-3.01 AND Zend-2.0 AND BSD-2-Clause AND MIT AND Apache-1.0 AND NCSA AND BSL-1.0
URL:       https://www.php.net/
Source0:   https://www.php.net/distributions/php-%{upver}.tar.xz
Source1:   php.conf
Source2:   php.ini
Source3:   macros.php
Source4:   php-fpm.conf
Source5:   php-fpm-www.conf
Source6:   php-fpm.service
Source7:   php-fpm.logrotate
Source8:   php.modconf
Source9:   php-fpm.wants
Source10:  nginx-fpm.conf
Source11:  10-opcache.ini
Source12:  opcache-default.blacklist
Source13:  20-ffi.ini
Source14:  nginx-php.conf
Source15:  https://www.php.net/distributions/php-keyring.gpg


Patch0:    php-7.4.0-httpd.patch
Patch1:    php-7.2.0-includedir.patch
Patch2:    php-8.0.0-embed.patch
Patch3:    php-8.1.0-libdb.patch

Patch4:    php-8.3.3-parser.patch
Patch5:    php-8.3.11-systzdata-v24.patch

Patch6:    php-7.4.0-phpize.patch
Patch7:    php-7.4.0-ldap_r.patch
Patch8:    php-8.3.13-phpinfo.patch
Patch9:    php-8.3.0-openssl-ec-param.patch
Patch10:   0001-add-sw_64-support.patch


BuildRequires: bzip2-devel
BuildRequires: pkgconfig(libcurl)  >= 7.29.0
BuildRequires: httpd-devel >= 2.0.46-1
BuildRequires: pam-devel
BuildRequires: httpd-filesystem
BuildRequires: nginx-filesystem
BuildRequires: libstdc++-devel
BuildRequires: openssl-devel >= 1.0.2
BuildRequires: pkgconfig(sqlite3) >= 3.7.4
BuildRequires: pkgconfig(zlib) >= 1.2.0.4
BuildRequires: smtpdaemon
BuildRequires: pkgconfig(libedit)
BuildRequires: pkgconfig(libpcre2-8) >= 10.30
BuildRequires: pkgconfig(libxcrypt)
BuildRequires: bzip2
BuildRequires: perl-interpreter
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: make
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: libtool
BuildRequires: libtool-ltdl-devel
BuildRequires: systemtap-sdt-devel
BuildRequires: %{_bindir}/ps
BuildRequires: tzdata

%if %{with_zts}
Provides: php-zts = %{version}-%{release}
Provides: php-zts%{?_isa} = %{version}-%{release}
%endif

%if %{with_modphp}
Requires: httpd-mmn = %{_httpd_mmn}
Provides: mod_php                = %{version}-%{release}
Requires(pre): httpd-filesystem
Provides: php(httpd)
Provides: deprecated()
%else
Recommends: httpd
%endif
Requires: php-common%{?_isa}     = %{version}-%{release}
Recommends: php-cli%{?_isa}      = %{version}-%{release}
Recommends: php-fpm%{?_isa}      = %{version}-%{release}
Recommends: php-mbstring%{?_isa} = %{version}-%{release}
Recommends: php-opcache%{?_isa}  = %{version}-%{release}
Recommends: php-pdo%{?_isa}      = %{version}-%{release}
%if %{with_sodium}
Recommends: php-sodium%{?_isa}   = %{version}-%{release}
%endif
Recommends: php-xml%{?_isa}      = %{version}-%{release}


%description
PHP is an HTML-embedded scripting language. PHP attempts to make it
easy for developers to write dynamically generated web pages. PHP also
offers built-in database integration for several commercial and
non-commercial database management systems, so writing a
database-enabled webpage with PHP is fairly simple. The most common
use of PHP coding is probably as a replacement for CGI scripts.
%if %{with_modphp}
The php package contains the module (often referred to as mod_php)
which adds support for the PHP language to Apache HTTP Server when
running in prefork mode. This module is deprecated.
%endif

%package cli
Summary: Command-line interface for PHP
License: PHP-3.01 AND Zend-2.0 AND BSD-2-Clause AND MIT AND Apache-1.0 AND NCSA AND PostgreSQL
Requires: php-common%{?_isa} = %{version}-%{release}
Provides: php-cgi = %{version}-%{release}, php-cgi%{?_isa} = %{version}-%{release}
Provides: php-pcntl, php-pcntl%{?_isa}
Provides: php-readline, php-readline%{?_isa}

%description cli
The php-cli package contains the command-line interface
executing PHP scripts, /usr/bin/php, and the CGI interface.


%package dbg
Summary: The interactive PHP debugger
Requires: php-common%{?_isa} = %{version}-%{release}

%description dbg
The php-dbg package contains the interactive PHP debugger.


%package fpm
Summary: PHP FastCGI Process Manager
BuildRequires: libacl-devel
BuildRequires: pkgconfig(libsystemd) >= 209
BuildRequires: pkgconfig(libselinux)
Requires: php-common%{?_isa} = %{version}-%{release}
%{?systemd_requires}
Requires(pre): httpd-filesystem
Requires: httpd-filesystem >= 2.4.10
Provides: php(httpd)
Requires: nginx-filesystem

%description fpm
PHP-FPM (FastCGI Process Manager) is an alternative PHP FastCGI
implementation with some additional features useful for sites of
any size, especially busier sites.

%package common
Summary: Common files for PHP
License: PHP-3.01 AND BSD-2-Clause
Provides: php(api) = %{apiver}-%{__isa_bits}
Provides: php(zend-abi) = %{zendver}-%{__isa_bits}
Provides: php(language) = %{version}, php(language)%{?_isa} = %{version}
Provides: php-bz2, php-bz2%{?_isa}
Provides: php-calendar, php-calendar%{?_isa}
Provides: php-core = %{version}, php-core%{?_isa} = %{version}
Provides: php-ctype, php-ctype%{?_isa}
Provides: php-curl, php-curl%{?_isa}
Provides: php-date, php-date%{?_isa}
Provides: bundled(timelib)
Provides: php-exif, php-exif%{?_isa}
Provides: php-fileinfo, php-fileinfo%{?_isa}
Provides: bundled(libmagic) = 5.29
Provides: php-filter, php-filter%{?_isa}
Provides: php-ftp, php-ftp%{?_isa}
Provides: php-gettext, php-gettext%{?_isa}
Provides: php-hash, php-hash%{?_isa}
Provides: php-mhash = %{version}, php-mhash%{?_isa} = %{version}
Provides: php-iconv, php-iconv%{?_isa}
Obsoletes: php-json < 8
Provides: php-json = %{version}, php-json%{?_isa} = %{version}
Provides: php-libxml, php-libxml%{?_isa}
Provides: php-openssl, php-openssl%{?_isa}
Provides: php-phar, php-phar%{?_isa}
Provides: php-pcre, php-pcre%{?_isa}
Provides: php-random, php-random%{?_isa}
Provides: php-reflection, php-reflection%{?_isa}
Provides: php-session, php-session%{?_isa}
Provides: php-sockets, php-sockets%{?_isa}
Provides: php-spl, php-spl%{?_isa}
Provides: php-standard = %{version}, php-standard%{?_isa} = %{version}
Provides: php-tokenizer, php-tokenizer%{?_isa}
Provides: php-zlib, php-zlib%{?_isa}

%description common
The php-common package contains files used by both the php
package and the php-cli package.

%package devel
Summary: Files needed for building PHP extensions
Requires: php-cli%{?_isa} = %{version}-%{release}
Requires: autoconf
Requires: automake
Requires: make
Requires: gcc
Requires: gcc-c++
Requires: libtool
Requires: krb5-devel%{?_isa}
Requires: libxml2-devel%{?_isa}
Requires: openssl-devel%{?_isa} >= 1.0.2
Requires: pcre2-devel%{?_isa}
Requires: zlib-devel%{?_isa}
%if %{with_zts}
Provides: php-zts-devel = %{version}-%{release}
Provides: php-zts-devel%{?_isa} = %{version}-%{release}
%endif
Recommends: php-nikic-php-parser4 >= 4.15.1


%description devel
The php-devel package contains the files needed for building PHP
extensions. If you need to compile your own PHP extensions, you will
need to install this package.

%package opcache
Summary:   The Zend OPcache
License:   PHP-3.01
Requires:  php-common%{?_isa} = %{version}-%{release}
Provides:  php-pecl-zendopcache = %{version}
Provides:  php-pecl-zendopcache%{?_isa} = %{version}
Provides:  php-pecl(opcache) = %{version}
Provides:  php-pecl(opcache)%{?_isa} = %{version}

%description opcache
The Zend OPcache provides faster PHP execution through opcode caching and
optimization. It improves PHP performance by storing precompiled script
bytecode in the shared memory. This eliminates the stages of reading code from
the disk and compiling it on future access. In addition, it applies a few
bytecode optimization patterns that make code execution faster.

%if %{with_imap}
%package imap
Summary: A module for PHP applications that use IMAP
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: pkgconfig(krb5)
BuildRequires: pkgconfig(krb5-gssapi)
BuildRequires: openssl-devel >= 1.0.2
BuildRequires: libc-client-devel

%description imap
The php-imap module will add IMAP (Internet Message Access Protocol)
support to PHP. IMAP is a protocol for retrieving and uploading e-mail
messages on mail servers. PHP is an HTML-embedded scripting language.
%endif

%package ldap
Summary: A module for PHP applications that use LDAP
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: pkgconfig(libsasl2)
BuildRequires: openldap-devel
BuildRequires: openssl-devel >= 1.0.2

%description ldap
The php-ldap adds Lightweight Directory Access Protocol (LDAP)
support to PHP. LDAP is a set of protocols for accessing directory
services over the Internet. PHP is an HTML-embedded scripting
language.

%package pdo
Summary: A database access abstraction module for PHP applications
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
Provides: php-pdo-abi  = %{pdover}-%{__isa_bits}
Provides: php(pdo-abi) = %{pdover}-%{__isa_bits}
Provides: php-sqlite3, php-sqlite3%{?_isa}
Provides: php-pdo_sqlite, php-pdo_sqlite%{?_isa}

%description pdo
The php-pdo package contains a dynamic shared object that will add
a database access abstraction layer to PHP.  This module provides
a common interface for accessing MySQL, PostgreSQL or other
databases.

%package mysqlnd
Summary: A module for PHP applications that use MySQL databases
License: PHP-3.01
Requires: php-pdo%{?_isa} = %{version}-%{release}
Provides: php_database
Provides: php-mysqli = %{version}-%{release}
Provides: php-mysqli%{?_isa} = %{version}-%{release}
Provides: php-pdo_mysql, php-pdo_mysql%{?_isa}

%description mysqlnd
The php-mysqlnd package contains a dynamic shared object that will add
MySQL database support to PHP. MySQL is an object-relational database
management system. PHP is an HTML-embeddable scripting language. If
you need MySQL support for PHP applications, you will need to install
this package and the php package.

This package use the MySQL Native Driver

%package pgsql
Summary: A PostgreSQL database module for PHP
License: PHP-3.01
Requires: php-pdo%{?_isa} = %{version}-%{release}
Provides: php_database
Provides: php-pdo_pgsql, php-pdo_pgsql%{?_isa}
BuildRequires: krb5-devel
BuildRequires: openssl-devel >= 1.0.2
BuildRequires: libpq-devel

%description pgsql
The php-pgsql package add PostgreSQL database support to PHP.
PostgreSQL is an object-relational database management
system that supports almost all SQL constructs. PHP is an
HTML-embedded scripting language. If you need back-end support for
PostgreSQL, you should install this package in addition to the main
php package.

%package process
Summary: Modules for PHP script using system process interfaces
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
Provides: php-posix, php-posix%{?_isa}
Provides: php-shmop, php-shmop%{?_isa}
Provides: php-sysvsem, php-sysvsem%{?_isa}
Provides: php-sysvshm, php-sysvshm%{?_isa}
Provides: php-sysvmsg, php-sysvmsg%{?_isa}

%description process
The php-process package contains dynamic shared objects which add
support to PHP using system interfaces for inter-process
communication.

%package odbc
Summary: A module for PHP applications that use ODBC databases
License: PHP-3.01
Requires: php-pdo%{?_isa} = %{version}-%{release}
Provides: php_database
Provides: php-pdo_odbc, php-pdo_odbc%{?_isa}
BuildRequires: unixODBC-devel

%description odbc
The php-odbc package contains a dynamic shared object that will add
database support through ODBC to PHP. ODBC is an open specification
which provides a consistent API for developers to use for accessing
data sources (which are often, but not always, databases). PHP is an
HTML-embeddable scripting language. If you need ODBC support for PHP
applications, you will need to install this package and the php
package.

%package soap
Summary: A module for PHP applications that use the SOAP protocol
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: pkgconfig(libxml-2.0)

%description soap
The php-soap package contains a dynamic shared object that will add
support to PHP for using the SOAP web services protocol.

%if %{with_firebird}
%package pdo-firebird
Summary: PDO driver for Interbase/Firebird databases
License: PHP-3.01
BuildRequires:  firebird-devel
Requires: php-pdo%{?_isa} = %{version}-%{release}
Provides: php_database
Provides: php-pdo_firebird, php-pdo_firebird%{?_isa}

%description pdo-firebird
The php-pdo-firebird package contains the PDO driver for
Interbase/Firebird databases.
%endif

%package snmp
Summary: A module for PHP applications that query SNMP-managed devices
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}, net-snmp
BuildRequires: net-snmp-devel

%description snmp
The php-snmp package contains a dynamic shared object that will add
support for querying SNMP devices to PHP.  PHP is an HTML-embeddable
scripting language. If you need SNMP support for PHP applications, you
will need to install this package and the php package.

%package xml
Summary: A module for PHP applications which use XML
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
Provides: php-dom, php-dom%{?_isa}
Provides: php-domxml, php-domxml%{?_isa}
Provides: php-simplexml, php-simplexml%{?_isa}
Provides: php-xmlreader, php-xmlreader%{?_isa}
Provides: php-xmlwriter, php-xmlwriter%{?_isa}
Provides: php-xsl, php-xsl%{?_isa}
BuildRequires: pkgconfig(libxslt)  >= 1.1
BuildRequires: pkgconfig(libexslt)
BuildRequires: pkgconfig(libxml-2.0)  >= 2.7.6

%description xml
The php-xml package contains dynamic shared objects which add support
to PHP for manipulating XML documents using the DOM tree,
and performing XSL transformations on XML documents.

%package mbstring
Summary: A module for PHP applications which need multi-byte string handling
License: PHP-3.01 AND LGPL-2.1-only AND OLDAP-2.8
BuildRequires: pkgconfig(oniguruma) >= 6.8
Provides: bundled(libmbfl) = 1.3.2
Requires: php-common%{?_isa} = %{version}-%{release}

%description mbstring
The php-mbstring package contains a dynamic shared object that will add
support for multi-byte string handling to PHP.

%package gd
Summary: A module for PHP applications for using the gd graphics library
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: pkgconfig(gdlib) >= 2.1.1

%description gd
The php-gd package contains a dynamic shared object that will add
support for using the gd graphics library to PHP.

%package bcmath
Summary: A module for PHP applications for using the bcmath library
License: PHP-3.01 AND LGPL-2.1-or-later
Requires: php-common%{?_isa} = %{version}-%{release}

%description bcmath
The php-bcmath package contains a dynamic shared object that will add
support for using the bcmath library to PHP.

%package gmp
Summary: A module for PHP applications for using the GNU MP library
License: PHP-3.01
BuildRequires: gmp-devel
Requires: php-common%{?_isa} = %{version}-%{release}

%description gmp
These functions allow you to work with arbitrary-length integers
using the GNU MP library.

%package dba
Summary: A database abstraction layer module for PHP applications
License: PHP-3.01
%if %{with_db4}
BuildRequires: libdb-devel
%endif
BuildRequires: tokyocabinet-devel
%if %{with_lmdb}
BuildRequires: lmdb-devel
%endif
%if %{with_qdbm}
BuildRequires: qdbm-devel
%endif
Requires: php-common%{?_isa} = %{version}-%{release}

%description dba
The php-dba package contains a dynamic shared object that will add
support for using the DBA database abstraction layer to PHP.

%if %{with_tidy}
%package tidy
Summary: Standard PHP module provides tidy library support
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: libtidy-devel

%description tidy
The php-tidy package contains a dynamic shared object that will add
support for using the tidy library to PHP.
%endif

%if %{with_freetds}
%package pdo-dblib
Summary: PDO driver for Microsoft SQL Server and Sybase databases
License: PHP-3.01
Requires: php-pdo%{?_isa} = %{version}-%{release}
BuildRequires: freetds-devel
Provides: php-pdo_dblib, php-pdo_dblib%{?_isa}

%description pdo-dblib
The php-pdo-dblib package contains a dynamic shared object
that implements the PHP Data Objects (PDO) interface to enable access from
PHP to Microsoft SQL Server and Sybase databases through the FreeTDS library.
%endif

%package embedded
Summary: PHP library for embedding in applications
Requires: php-common%{?_isa} = %{version}-%{release}
Provides: php-embedded-devel = %{version}-%{release}
Provides: php-embedded-devel%{?_isa} = %{version}-%{release}

%description embedded
The php-embedded package contains a library which can be embedded
into applications to provide PHP scripting language support.

%if %{with_pspell}
%package pspell
Summary: A module for PHP applications for using pspell interfaces
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: aspell-devel >= 0.50.0

%description pspell
The php-pspell package contains a dynamic shared object that will add
support for using the pspell library to PHP.
%endif

%package intl
Summary: Internationalization extension for PHP applications
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: pkgconfig(icu-i18n) >= 50.1
BuildRequires: pkgconfig(icu-io)   >= 50.1
BuildRequires: pkgconfig(icu-uc)   >= 50.1

%description intl
The php-intl package contains a dynamic shared object that will add
support for using the ICU library to PHP.

%package enchant
Summary: Enchant spelling extension for PHP applications
License: PHP-3.01
Requires: php-common%{?_isa} = %{version}-%{release}
BuildRequires: pkgconfig(enchant-2)

%description enchant
The php-enchant package contains a dynamic shared object that will add
support for using the enchant library to PHP.

%if %{with_sodium}
%package sodium
Summary: Wrapper for the Sodium cryptographic library
License: PHP-3.01
BuildRequires:  pkgconfig(libsodium) >= 1.0.9

Requires: php-common%{?_isa} = %{version}-%{release}
Obsoletes: php-pecl-libsodium2 < 3
Provides:  php-pecl(libsodium)         = %{version}
Provides:  php-pecl(libsodium)%{?_isa} = %{version}

%description sodium
The php-sodium package provides a simple,
low-level PHP extension for the libsodium cryptographic library.
%endif


%package ffi
Summary: Foreign Function Interface
License: PHP-3.01
Group: System Environment/Libraries
BuildRequires:  pkgconfig(libffi)
Requires: php-common%{?_isa} = %{version}-%{release}

%description ffi
FFI is one of the features that made Python and LuaJIT very useful for fast
prototyping. It allows calling C functions and using C data types from pure
scripting language and therefore develop “system code” more productively.

For PHP, FFI opens a way to write PHP extensions and bindings to C libraries
in pure PHP.

%package_help

%prep
%autosetup -n php-%{upver} -p1

cp Zend/LICENSE ZEND_LICENSE
cp TSRM/LICENSE TSRM_LICENSE
cp Zend/asm/LICENSE BOOST_LICENSE
cp sapi/fpm/LICENSE fpm_LICENSE
cp ext/mbstring/libmbfl/LICENSE libmbfl_LICENSE
cp ext/bcmath/libbcmath/LICENSE libbcmath_LICENSE
cp ext/date/lib/LICENSE.rst timelib_LICENSE

mkdir build-cgi build-embedded \
%if %{with_modphp}
    build-apache \
%endif
%if %{with_zts}
    build-zts build-ztscli \
%endif
    build-fpm


rm ext/date/tests/timezone_location_get.phpt
rm ext/date/tests/timezone_version_get.phpt
rm ext/date/tests/timezone_version_get_basic1.phpt
rm ext/sockets/tests/mcast_ipv?_recv.phpt
rm Zend/tests/bug54268.phpt
rm Zend/tests/bug68412.phpt
rm ext/zlib/tests/004-mb.phpt

pver=$(sed -n '/#define PHP_VERSION /{s/.* "//;s/".*$//;p}' main/php_version.h)
if test "x${pver}" != "x%{upver}"; then
   : Error: Upstream PHP version is now ${pver}, expecting %{upver}.
   : Update the version macros and rebuild.
   exit 1
fi

vapi=`sed -n '/#define PHP_API_VERSION/{s/.* //;p}' main/php.h`
if test "x${vapi}" != "x%{apiver}"; then
   : Error: Upstream API version is now ${vapi}, expecting %{apiver}.
   : Update the apiver macro and rebuild.
   exit 1
fi

vzend=`sed -n '/#define ZEND_MODULE_API_NO/{s/^[^0-9]*//;p;}' Zend/zend_modules.h`
if test "x${vzend}" != "x%{zendver}"; then
   : Error: Upstream Zend ABI version is now ${vzend}, expecting %{zendver}.
   : Update the zendver macro and rebuild.
   exit 1
fi

vpdo=`sed -n '/#define PDO_DRIVER_API/{s/.*[ 	]//;p}' ext/pdo/php_pdo_driver.h`
if test "x${vpdo}" != "x%{pdover}"; then
   : Error: Upstream PDO ABI version is now ${vpdo}, expecting %{pdover}.
   : Update the pdover macro and rebuild.
   exit 1
fi

rm -f TSRM/tsrm_win32.h \
      TSRM/tsrm_config.w32.h \
      Zend/zend_config.w32.h \
      ext/mysqlnd/config-win.h \
      ext/standard/winver.h \
      main/win32_internal_function_disabled.h \
      main/win95nt.h

find . -name \*.[ch] -exec chmod 644 {} \;
chmod 644 README.*

cp %{SOURCE11} %{SOURCE12} %{SOURCE13} .


%build

%define _lto_cflags %{nil}

export SOURCE_DATE_EPOCH=$(date +%s -r NEWS)
export PHP_UNAME=$(uname)
export PHP_BUILD_SYSTEM=$(cat /etc/openEuler-release | sed -e 's/ Beta//')
%if 0%{?vendor:1}
export PHP_BUILD_PROVIDER="%{vendor}"
%endif
export PHP_BUILD_COMPILER="$(gcc --version | head -n1)"
export PHP_BUILD_ARCH="%{_arch}"

libtoolize --force --copy
cat `aclocal --print-ac-dir`/{libtool,ltoptions,ltsugar,ltversion,lt~obsolete}.m4 >build/libtool.m4

touch configure.ac
./buildconf --force

CFLAGS=$(echo $RPM_OPT_FLAGS -fno-strict-aliasing -Wno-pointer-sign | sed 's/-mstackrealign//')
export CFLAGS

EXTENSION_DIR=%{_libdir}/php/modules; export EXTENSION_DIR

PEAR_INSTALLDIR=%{_datadir}/pear; export PEAR_INSTALLDIR

build() {
mkdir Zend && cp ../Zend/zend_{language,ini}_{parser,scanner}.[ch] Zend

ln -sf ../configure
%configure \
    --enable-rtld-now \
    --cache-file=../config.cache \
    --with-libdir=%{_lib} \
    --with-config-file-path=%{_sysconfdir} \
    --with-config-file-scan-dir=%{_sysconfdir}/php.d \
    --disable-debug \
    --with-pic \
    --disable-rpath \
    --without-pear \
    --with-exec-dir=%{_bindir} \
    --without-gdbm \
    --with-openssl \
    --with-system-ciphers \
    --with-external-pcre \
    --with-external-libcrypt \
%ifarch s390 s390x sparc64 sparcv9 riscv64 loongarch64
    --without-pcre-jit \
%endif
    --with-zlib \
    --with-layout=GNU \
    --with-kerberos \
    --with-libxml \
    --with-system-tzdata \
    --with-mhash \
    --without-password-argon2 \
    --enable-dtrace \
    $*
if test $? != 0; then
  tail -500 config.log
  : configure failed
  exit 1
fi

%make_build
}

pushd build-cgi

build --libdir=%{_libdir}/php \
      --enable-pcntl \
      --enable-opcache \
      --enable-phpdbg \
%if %{with_imap}
      --with-imap=shared --with-imap-ssl \
%endif
      --enable-mbstring=shared \
      --enable-mbregex \
      --enable-gd=shared \
      --with-external-gd \
      --with-gmp=shared \
      --enable-calendar=shared \
      --enable-bcmath=shared \
      --with-bz2=shared \
      --enable-ctype=shared \
      --enable-dba=shared \
%if %{with_db4}
                          --with-db4=%{_prefix} \
%endif
                          --with-tcadb=%{_prefix} \
%if %{with_lmdb}
                          --with-lmdb=%{_prefix} \
%endif
%if %{with_qdbm}
                          --with-qdbm=%{_prefix} \
%endif
      --enable-exif=shared \
      --enable-ftp=shared \
      --with-gettext=shared \
      --with-iconv=shared \
      --enable-sockets=shared \
      --enable-tokenizer=shared \
      --with-ldap=shared --with-ldap-sasl \
      --enable-mysqlnd=shared \
      --with-mysqli=shared,mysqlnd \
      --with-mysql-sock=%{mysql_sock} \
%if %{with_firebird}
      --with-pdo-firebird=shared \
%endif
      --enable-dom=shared \
      --with-pgsql=shared \
      --enable-simplexml=shared \
      --enable-xml=shared \
      --with-snmp=shared,%{_prefix} \
      --enable-soap=shared \
      --with-xsl=shared,%{_prefix} \
      --enable-xmlreader=shared --enable-xmlwriter=shared \
      --with-curl=shared \
      --enable-pdo=shared \
      --with-pdo-odbc=shared,unixODBC,%{_prefix} \
      --with-pdo-mysql=shared,mysqlnd \
      --with-pdo-pgsql=shared,%{_prefix} \
      --with-pdo-sqlite=shared \
%if %{with_freetds}
      --with-pdo-dblib=shared,%{_prefix} \
%endif
      --with-sqlite3=shared \
      --without-readline \
      --with-libedit \
%if %{with_pspell}
      --with-pspell=shared \
%endif
      --enable-phar=shared \
%if %{with_tidy}
      --with-tidy=shared,%{_prefix} \
%endif
      --enable-sysvmsg=shared --enable-sysvshm=shared --enable-sysvsem=shared \
      --enable-shmop=shared \
      --enable-posix=shared \
      --with-unixODBC=shared,%{_prefix} \
      --enable-fileinfo=shared \
      --with-ffi=shared \
%if %{with_sodium}
      --with-sodium=shared \
%else
      --without-sodium \
%endif
      --enable-intl=shared \
      --with-enchant=shared
popd

without_shared="--without-gd \
      --disable-dom --disable-dba --without-unixODBC \
      --disable-opcache \
      --disable-phpdbg \
      --without-ffi \
      --disable-xmlreader --disable-xmlwriter \
      --without-sodium \
      --without-sqlite3 --disable-phar --disable-fileinfo \
      --without-pspell \
      --without-curl --disable-posix --disable-xml \
      --disable-simplexml --disable-exif --without-gettext \
      --without-iconv --disable-ftp --without-bz2 --disable-ctype \
      --disable-shmop --disable-sockets --disable-tokenizer \
      --disable-sysvmsg --disable-sysvshm --disable-sysvsem"

%if %{with_modphp}
pushd build-apache
build --with-apxs2=%{_httpd_apxs} \
      --libdir=%{_libdir}/php \
      --without-mysqli \
      --disable-pdo \
      ${without_shared}
popd
%endif

pushd build-fpm
build --enable-fpm \
      --with-fpm-acl \
      --with-fpm-systemd \
      --with-fpm-selinux \
      --libdir=%{_libdir}/php \
      --without-mysqli \
      --disable-pdo \
      ${without_shared}
popd

pushd build-embedded
build --enable-embed \
      --without-mysqli --disable-pdo \
      ${without_shared}
popd

%if %{with_zts}
pushd build-ztscli

EXTENSION_DIR=%{_libdir}/php-zts/modules
build --includedir=%{_includedir}/php-zts \
      --libdir=%{_libdir}/php-zts \
      --enable-zts \
      --program-prefix=zts- \
      --disable-cgi \
      --with-config-file-scan-dir=%{_sysconfdir}/php-zts.d \
      --enable-pcntl \
      --enable-opcache \
%if %{with_imap}
      --with-imap=shared --with-imap-ssl \
%endif
      --enable-mbstring=shared \
      --enable-mbregex \
      --enable-gd=shared \
      --with-external-gd \
      --with-gmp=shared \
      --enable-calendar=shared \
      --enable-bcmath=shared \
      --with-bz2=shared \
      --enable-ctype=shared \
      --enable-dba=shared \
%if %{with_db4}
                          --with-db4=%{_prefix} \
%endif
                          --with-tcadb=%{_prefix} \
%if %{with_lmdb}
                          --with-lmdb=%{_prefix} \
%endif
%if %{with_qdbm}
                          --with-qdbm=%{_prefix} \
%endif
      --with-gettext=shared \
      --with-iconv=shared \
      --enable-sockets=shared \
      --enable-tokenizer=shared \
      --enable-exif=shared \
      --enable-ftp=shared \
      --with-ldap=shared --with-ldap-sasl \
      --enable-mysqlnd=shared \
      --with-mysqli=shared,mysqlnd \
      --with-mysql-sock=%{mysql_sock} \
      --enable-mysqlnd-threading \
%if %{with_firebird}
      --with-pdo-firebird=shared \
%endif
      --enable-dom=shared \
      --with-pgsql=shared \
      --enable-simplexml=shared \
      --enable-xml=shared \
      --with-snmp=shared,%{_prefix} \
      --enable-soap=shared \
      --with-xsl=shared,%{_prefix} \
      --enable-xmlreader=shared --enable-xmlwriter=shared \
      --with-curl=shared \
      --enable-pdo=shared \
      --with-pdo-odbc=shared,unixODBC,%{_prefix} \
      --with-pdo-mysql=shared,mysqlnd \
      --with-pdo-pgsql=shared,%{_prefix} \
      --with-pdo-sqlite=shared \
%if %{with_freetds}
      --with-pdo-dblib=shared,%{_prefix} \
%endif
      --with-sqlite3=shared \
      --without-readline \
      --with-libedit \
%if %{with_pspell}
      --with-pspell=shared \
%endif
      --enable-phar=shared \
%if %{with_tidy}
      --with-tidy=shared,%{_prefix} \
%endif
      --enable-sysvmsg=shared --enable-sysvshm=shared --enable-sysvsem=shared \
      --enable-shmop=shared \
      --enable-posix=shared \
      --with-unixODBC=shared,%{_prefix} \
      --enable-fileinfo=shared \
      --with-ffi=shared \
%if %{with_sodium}
      --with-sodium=shared \
%else
      --without-sodium \
%endif
      --enable-intl=shared \
      --with-enchant=shared
popd

%endif


%check
: Ensure proper NTS/ZTS build
$RPM_BUILD_ROOT%{_bindir}/php     -n -v | grep NTS
%if %{with_zts}
$RPM_BUILD_ROOT%{_bindir}/zts-php -n -v | grep ZTS
%endif

%if %runselftest
cd build-fpm

export NO_INTERACTION=1 REPORT_EXIT_STATUS=1 MALLOC_CHECK_=2
export SKIP_ONLINE_TESTS=1
export SKIP_IO_CAPTURE_TESTS=1
unset TZ LANG LC_ALL
if ! make test TESTS=-j4; then
  set +x
  for f in $(find .. -name \*.diff -type f -print); do
    if ! grep -q XFAIL "${f/.diff/.phpt}"
    then
      echo "TEST FAILURE: $f --"
      cat "$f"
      echo -e "\n-- $f result ends."
    fi
  done
  set -x
  #exit 1
fi
unset NO_INTERACTION REPORT_EXIT_STATUS MALLOC_CHECK_
%endif

%install
%if %{with_zts}
make -C build-ztscli install \
     INSTALL_ROOT=$RPM_BUILD_ROOT
%endif

make -C build-embedded install-sapi install-headers \
     INSTALL_ROOT=$RPM_BUILD_ROOT

make -C build-fpm install-fpm \
     INSTALL_ROOT=$RPM_BUILD_ROOT

make -C build-cgi install \
     INSTALL_ROOT=$RPM_BUILD_ROOT

install -m 755 build-embedded/scripts/php-config $RPM_BUILD_ROOT%{_bindir}/php-config

install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/php.ini

install -m 755 -d $RPM_BUILD_ROOT%{_datadir}/php/preload

%if %{with_modphp}
install -m 755 -d $RPM_BUILD_ROOT%{_httpd_moddir}
install -m 755 build-apache/libs/libphp.so $RPM_BUILD_ROOT%{_httpd_moddir}
%endif

%if %{with_modphp}
install -D -m 644 %{SOURCE8} $RPM_BUILD_ROOT%{_httpd_modconfdir}/20-php.conf
%endif
install -D -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_httpd_confdir}/php.conf

install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/php.d
%if %{with_zts}
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d
%endif
install -m 755 -d $RPM_BUILD_ROOT%{_sharedstatedir}/php
install -m 755 -d $RPM_BUILD_ROOT%{_sharedstatedir}/php/peclxml
install -m 700 -d $RPM_BUILD_ROOT%{_sharedstatedir}/php/session
install -m 700 -d $RPM_BUILD_ROOT%{_sharedstatedir}/php/wsdlcache
install -m 700 -d $RPM_BUILD_ROOT%{_sharedstatedir}/php/opcache

install -m 755 -d $RPM_BUILD_ROOT%{_docdir}/pecl
install -m 755 -d $RPM_BUILD_ROOT%{_datadir}/tests/pecl

install -m 755 -d $RPM_BUILD_ROOT%{_localstatedir}/log/php-fpm
install -m 755 -d $RPM_BUILD_ROOT/run/php-fpm
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.d
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.conf
install -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.d/www.conf
mv $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.conf.default .
mv $RPM_BUILD_ROOT%{_sysconfdir}/php-fpm.d/www.conf.default .
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/systemd/system/php-fpm.service.d
install -Dm 644 %{SOURCE6}  $RPM_BUILD_ROOT%{_unitdir}/php-fpm.service
install -Dm 644 %{SOURCE9} $RPM_BUILD_ROOT%{_unitdir}/httpd.service.d/php-fpm.conf
install -Dm 644 %{SOURCE9} $RPM_BUILD_ROOT%{_unitdir}/nginx.service.d/php-fpm.conf
install -m 755 -d $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -m 644 %{SOURCE7} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/php-fpm
install -D -m 644 %{SOURCE10} $RPM_BUILD_ROOT%{_sysconfdir}/nginx/conf.d/php-fpm.conf
install -D -m 644 %{SOURCE14} $RPM_BUILD_ROOT%{_sysconfdir}/nginx/default.d/php.conf

TESTCMD="$RPM_BUILD_ROOT%{_bindir}/php --no-php-ini"
for mod in core date filter hash libxml openssl pcntl pcre readline reflection session spl standard zlib
do
     $TESTCMD --modules | grep -qi $mod
done

TESTCMD="$TESTCMD --define extension_dir=$RPM_BUILD_ROOT%{_libdir}/php/modules"

for mod in pgsql odbc ldap snmp \
%if %{with_imap}
    imap \
%endif
    mysqlnd mysqli \
    mbstring gd dom xsl soap bcmath dba \
    simplexml bz2 calendar ctype exif ftp gettext gmp iconv \
    sockets tokenizer opcache \
    sqlite3 \
    enchant phar fileinfo intl \
    ffi \
%if %{with_tidy}
    tidy \
%endif
%if %{with_pspell}
    pspell \
%endif
    curl \
%if %{with_sodium}
    sodium \
%endif
    posix shmop sysvshm sysvsem sysvmsg xml \
    pdo pdo_mysql pdo pdo_pgsql pdo_odbc pdo_sqlite \
%if %{with_firebird}
    pdo_firebird \
%endif
%if %{with_freetds}
    pdo_dblib \
%endif
    xmlreader xmlwriter
do
    case $mod in
      opcache)
        # Zend extensions
        TESTCMD="$TESTCMD --define zend_extension=$mod"
        ini=10-${mod}.ini;;
      pdo_*|mysqli|xmlreader)
        # Extensions with dependencies on 20-*
        TESTCMD="$TESTCMD --define extension=$mod"
        ini=30-${mod}.ini;;
      *)
        # Extensions with no dependency
        TESTCMD="$TESTCMD --define extension=$mod"
        ini=20-${mod}.ini;;
    esac

    $TESTCMD --modules | grep -qi $mod

    if [ -f ${ini} ]; then
      cp -p ${ini} $RPM_BUILD_ROOT%{_sysconfdir}/php.d/${ini}
%if %{with_zts}
      cp -p ${ini} $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d/${ini}
%endif
    else
      cat > $RPM_BUILD_ROOT%{_sysconfdir}/php.d/${ini} <<EOF
; Enable ${mod} extension module
extension=${mod}
EOF
%if %{with_zts}
      cat > $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d/${ini} <<EOF
; Enable ${mod} extension module
extension=${mod}
EOF
%endif
    fi
    cat > files.${mod} <<EOF
%{_libdir}/php/modules/${mod}.so
%config(noreplace) %{_sysconfdir}/php.d/${ini}
%if %{with_zts}
%{_libdir}/php-zts/modules/${mod}.so
%config(noreplace) %{_sysconfdir}/php-zts.d/${ini}
%endif
EOF
done

cat files.dom files.xsl files.xml{reader,writer} \
    files.simplexml >> files.xml

cat files.mysqli \
    files.pdo_mysql \
    >> files.mysqlnd

cat files.pdo_pgsql >> files.pgsql
cat files.pdo_odbc >> files.odbc

cat files.shmop files.sysv* files.posix > files.process

cat files.pdo_sqlite >> files.pdo
cat files.sqlite3 >> files.pdo

cat files.curl files.phar files.fileinfo \
    files.exif files.gettext files.iconv files.calendar \
    files.ftp files.bz2 files.ctype files.sockets \
    files.tokenizer > files.common

install -m 644 %{SOURCE12} $RPM_BUILD_ROOT%{_sysconfdir}/php.d/opcache-default.blacklist
%if %{with_zts}
install -m 644 %{SOURCE12} $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d/opcache-default.blacklist
sed -e '/blacklist_filename/s/php.d/php-zts.d/' \
    -i $RPM_BUILD_ROOT%{_sysconfdir}/php-zts.d/10-opcache.ini
%endif

sed -e "s/@PHP_APIVER@/%{apiver}-%{__isa_bits}/" \
    -e "s/@PHP_ZENDVER@/%{zendver}-%{__isa_bits}/" \
    -e "s/@PHP_PDOVER@/%{pdover}-%{__isa_bits}/" \
    -e "s/@PHP_VERSION@/%{upver}/" \
%if ! %{with_zts}
    -e "/zts/d" \
%endif
    < %{SOURCE3} > macros.php
install -m 644 -D macros.php \
           $RPM_BUILD_ROOT%{_rpmmacrodir}/macros.php

rm -rf $RPM_BUILD_ROOT%{_libdir}/php/modules/*.a \
       $RPM_BUILD_ROOT%{_libdir}/php-zts/modules/*.a \
       $RPM_BUILD_ROOT%{_bindir}/{phptar} \
       $RPM_BUILD_ROOT%{_datadir}/pear \
       $RPM_BUILD_ROOT%{_bindir}/zts-phar* \
       $RPM_BUILD_ROOT%{_mandir}/man1/zts-phar* \
       $RPM_BUILD_ROOT%{_libdir}/libphp.a \
       $RPM_BUILD_ROOT%{_libdir}/libphp.la

rm -f README.{Zeus,QNX,CVS-RULES}


%post fpm
%systemd_post php-fpm.service

%preun fpm
%systemd_preun php-fpm.service

%transfiletriggerin fpm -- %{_sysconfdir}/php-fpm.d %{_sysconfdir}/php.d
systemctl try-restart php-fpm.service >/dev/null 2>&1 || :


%files
%if %{with_modphp}
%{_httpd_moddir}/libphp.so
%config(noreplace) %{_httpd_modconfdir}/20-php.conf
%attr(0770,root,apache) %dir %{_sharedstatedir}/php/session
%attr(0770,root,apache) %dir %{_sharedstatedir}/php/wsdlcache
%attr(0770,root,apache) %dir %{_sharedstatedir}/php/opcache
%config(noreplace) %{_httpd_confdir}/php.conf
%endif

%files common -f files.common
%license LICENSE TSRM_LICENSE ZEND_LICENSE BOOST_LICENSE
%license timelib_LICENSE
%config(noreplace) %{_sysconfdir}/php.ini
%dir %{_sysconfdir}/php.d
%dir %{_libdir}/php
%dir %{_libdir}/php/modules
%if %{with_zts}
%dir %{_sysconfdir}/php-zts.d
%dir %{_libdir}/php-zts
%dir %{_libdir}/php-zts/modules
%endif
%dir %{_sharedstatedir}/php
%dir %{_sharedstatedir}/php/peclxml
%dir %{_datadir}/php
%dir %{_docdir}/pecl
%dir %{_datadir}/tests
%dir %{_datadir}/tests/pecl

%files cli
%{_bindir}/php
%if %{with_zts}
%{_bindir}/zts-php
%endif
%{_bindir}/php-cgi
%{_bindir}/phar.phar
%{_bindir}/phar
%{_bindir}/phpize


%files dbg
%doc sapi/phpdbg/CREDITS
%{_bindir}/phpdbg
%if %{with_zts}
%{_bindir}/zts-phpdbg
%endif

%files fpm
%license fpm_LICENSE
%attr(0770,root,apache) %dir %{_sharedstatedir}/php/session
%attr(0770,root,apache) %dir %{_sharedstatedir}/php/wsdlcache
%attr(0770,root,apache) %dir %{_sharedstatedir}/php/opcache
%config(noreplace) %{_httpd_confdir}/php.conf
%config(noreplace) %{_sysconfdir}/php-fpm.conf
%config(noreplace) %{_sysconfdir}/php-fpm.d/www.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/php-fpm
%config(noreplace) %{_sysconfdir}/nginx/conf.d/php-fpm.conf
%config(noreplace) %{_sysconfdir}/nginx/default.d/php.conf
%{_unitdir}/php-fpm.service
%{_unitdir}/httpd.service.d/php-fpm.conf
%{_unitdir}/nginx.service.d/php-fpm.conf
%{_sbindir}/php-fpm
%dir %{_sysconfdir}/systemd/system/php-fpm.service.d
%dir %{_sysconfdir}/php-fpm.d
# log owned by apache for log
%attr(770,apache,root) %dir %{_localstatedir}/log/php-fpm
%dir %ghost /run/php-fpm
%dir %{_datadir}/fpm
%{_datadir}/fpm/status.html

%files devel
%{_bindir}/php-config
%{_includedir}/php
%{_libdir}/php/build
%if %{with_zts}
%{_bindir}/zts-php-config
%{_bindir}/zts-phpize
%{_includedir}/php-zts
%{_libdir}/php-zts/build
%endif
%{_rpmmacrodir}/macros.php

%files embedded
%{_libdir}/libphp.so
%{_libdir}/libphp-%{embed_version}.so

%files pgsql -f files.pgsql
%files odbc -f files.odbc
%if %{with_imap}
%files imap -f files.imap
%endif
%files ldap -f files.ldap
%files snmp -f files.snmp
%files xml -f files.xml
%files mbstring -f files.mbstring
%license libmbfl_LICENSE
%files gd -f files.gd
%files soap -f files.soap
%files bcmath -f files.bcmath
%license libbcmath_LICENSE
%files gmp -f files.gmp
%files dba -f files.dba
%files pdo -f files.pdo
%if %{with_tidy}
%files tidy -f files.tidy
%endif
%if %{with_freetds}
%files pdo-dblib -f files.pdo_dblib
%endif
%if %{with_pspell}
%files pspell -f files.pspell
%endif
%files intl -f files.intl
%files process -f files.process
%if %{with_firebird}
%files pdo-firebird -f files.pdo_firebird
%endif
%files enchant -f files.enchant
%files mysqlnd -f files.mysqlnd
%files opcache -f files.opcache
%config(noreplace) %{_sysconfdir}/php.d/opcache-default.blacklist
%if %{with_zts}
%config(noreplace) %{_sysconfdir}/php-zts.d/opcache-default.blacklist
%endif
%if %{with_sodium}
%files sodium -f files.sodium
%endif
%files ffi -f files.ffi
%dir %{_datadir}/php/preload

%files help
%doc EXTENSIONS NEWS README* UPGRADING* *md docs
%doc php-fpm.conf.default www.conf.default php.ini-*
%{_mandir}/man?/*

%changelog
* Tue Feb 25 2025 zhangshaoning <zhangshaoning@uniontech.com> - 8.3.17-2
- Add sw_64 support

* Wed Feb 12 2025 Funda Wang <fundawang@yeah.net> - 8.3.17-1
- New version 8.3.17

* Wed Jan 15 2025 Funda Wang <fundawang@yeah.net> - 8.3.16-1
- New version 8.3.16

* Wed Dec 18 2024 Funda Wang <fundawang@yeah.net> - 8.3.15-1
- New version 8.3.15

* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 8.3.14-1
- New version 8.3.14

* Wed Oct 23 2024 Funda Wang <fundawang@yeah.net> - 8.3.13-1
- New version 8.3.13

* Wed Sep 25 2024 Funda Wang <fundawang@yeah.net> - 8.3.12-1
- New version 8.3.12

* Mon Sep 02 2024 Funda Wang <fundawang@yeah.net> - 8.3.11-1
- New version 8.3.11

* Wed Jul 31 2024 Funda Wang <fundawang@yeah.net> - 8.3.10-1
- New version 8.3.10

* Wed Jun 12 2024 Funda Wang <fundawang@yeah.net> - 8.3.8-2
- Fix GH-14480 Method visibility issue introduced in version 8.3.8
- Update licenses declaration

* Fri Jun 07 2024 Funda Wang <fundawang@yeah.net> - 8.3.8-1
- New version 8.3.8

* Fri Apr 12 2024 Funda Wang <fundawang@yeah.net> - 8.3.6-1
- New version 8.3.6

* Sat Mar 2 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 8.3.3-1
- New version 8.3.3 

* Sat Oct 28 2023 Funda Wang <fundawang@yeah.net> - 8.2.12-1
- New version 8.2.12

* Fri Sep 29 2023 Funda Wang <fundawang@yeah.net> - 8.2.11-1
- New version 8.2.11

* Sun Sep 3 2023 Funda Wang <fundawang@yeah.net> - 8.2.10-1
- New version 8.2.10

* Fri Aug 18 2023 Funda Wang <fundawang@yeah.net> - 8.2.9-2
- Refresh tarball with upstream

* Tue Aug 1 2023 Funda Wang <fundawang@yeah.net> - 8.2.9-1
- update to 8.2.9

* Fri Jun 16 2023  Dillon Chen <dillon.chen@gmail.com> - 8.2.8-1
- update to 8.2.8

* Fri Jun 16 2023  Dillon Chen <dillon.chen@gmail.com> - 8.2.7-1
- update to 8.2.7

* Mon May  8 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 8.2.3-2
- disable pcre-jit for loongarch64

* Tue Feb 21 2023 Dillon Chen <dillon.chen@gmail.com> - 8.2.3-1
- CVE-2023-0567,CVE-2023-0568,CVE-2023-0662
- update to 8.2.3

* Mon Jan 16 2023 Dillon Chen <dillon.chen@gmail.com> - 8.2.1-1
- update to 8.2.1

* Tue Dec 27 2022 huyab<1229981468@qq.com> - 8.2.0-1
- update version to 8.2.0-1

* Thu Nov 24 2022 Funda Wang <fundawang@yeah.net> - 8.1.13-1
- New version 8.1.13

* Sat Oct 29 2022 Funda Wang <fundawang@yeah.net> - 8.1.12-1
- New version 8.1.12

* Thu Sep 29 2022 dillon chen <dillon.chen@gmail.com> - 8.1.11-1
- update to 8.1.11 
- CVE-2022-31628, CVE-2022-31629

* Sat Sep 17 2022 Funda Wang <fundawang@yeah.net> - 8.1.10-1
- New version 8.1.10
- Sync systzdata with remi's php repo
- enable libsodium sub package
- cleanup unused build switches

* Tue Jul 12 2022 Hugel <gengqihu1@h-partners.com> - 8.1.1-5
- Fix CVE-2022-31627

* Sat Jun 18 2022 Hugel <gengqihu1@h-partners.com> - 8.1.1-4
- Fix CVE-2022-31625 CVE-2022-31626

* Wed Mar 9 2022 panxiaohe <panxh.life@foxmail.com> - 8.1.1-3
- Fix CVE-2021-21708

* Wed Feb 23 2022 panxiaohe <panxh.life@foxmail.com> - 8.1.1-2
- use lmdb instead of Berkeley DB for package dba
- update %%configure for riscv 

* Thu Dec 30 2021 panxiaohe <panxiaohe@huawei.com> - 8.1.1-1
- Update to 8.1.1

* Sat Dec 18 2021 yixiangzhike <yixiangzhike007@163.com> - 8.0.0-8
- Fix bugs found by oss-fuzz

* Thu Dec 2 2021 fuanan <fuanan3@huawei.com> - 8.0.0-7
- Fix CVE-2021-21707

* Thu Nov 4 2021 panxiaohe <panxiaohe@huawei.com> - 8.0.0-6
- Fix CVE-2021-21703

* Sat Oct 16 2021 wangjie <wangjie375@huawei.com> - 8.0.0-5
- fix CVE-2021-21704

* Wed Sep 29 2021 fuanan <fuanan3@huawei.com> - 8.0.0-4
- refix CVE-2020-7071 and fix CVE-2021-21705

* Tue Jun 1 2021 guoxiaoqi <guoxiaoqi2@huawei.com> - 8.0.0-3
- Fix CVE-2021-21702

* Fri Jan 29 2021 panxiaohe <panxiaohe@huawei.com> - 8.0.0-2
- Fix CVE-2020-7071 

* Thu Dec 31 2020 panxiaohe <panxiaohe@huawei.com> - 8.0.0-1
- Update to 8.0.0

* Mon Sep 21 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 7.2.10-6
- Fix CVE-2020-7068

* Tue Jul 21 2020 wangyue <wangyue92@huawei.com> - 7.2.10-5
- Type:cves
- ID:CVE-2019-11048
- SUG:restart
- DESC:fix CVE-2019-11048

* Fri Apr 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.2.10-4
- Type:cves
- ID:CVE-2020-7064 CVE-2020-7066
- SUG:restart
- DESC:fix CVE-2020-7064 CVE-2020-7066

* Mon Mar 16 2020 shijian <shijian16@huawei.com> - 7.2.10-3
- Type:cves
- ID:CVE-2018-19518 CVE-2019-6977
- SUG:restart
- DESC:fix CVE-2018-19518 CVE-2019-6977

* Thu Mar 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.2.10-2
- Add CVE patches
- Fix CVE-2019-9021 CVE-2019-9022 CVE-2019-9023 CVE-2019-9024
  CVE-2019-9637 CVE-2019-9638 CVE-2019-9639 CVE-2019-9640
  CVE-2018-20783 CVE-2019-9641 CVE-2019-11034 CVE-2019-11035
  CVE-2019-11036 CVE-2019-11041 CVE-2019-11042 CVE-2019-11043
  CVE-2018-19935 CVE-2019-11045 CVE-2019-11046 CVE-2019-11050
  CVE-2019-11047

* Fri Feb 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 7.2.10-1
- Package init
